#NOTES
#AES - block encryption. make padding/unpadding fcns to make plaintext 16byte blocks. Key must be tuple, 16 bits. Recommended # of iterations in tutorial 16384. 
#salt must be random, NOT just unique (do not use rand fcn). public, nonsecret. pass in ciphertext. min of 16bytes.
# key comes from 1pass
# cipher from cryptoplus

#####################################

import unittest
import getpass
import sys, traceback
import Crypto.Random.OSRNG.posix as RNG
from onepassword import _pbkdf2_cryptography
from CryptoPlus.Cipher import AES

#####################################

salt_len=32

#####################################

def pad_data(data): #method from tutorial

        # return data if no padding is required
        if len(data) % 16 == 0:
                return data

        # subtract one byte that should be the 0x80
        # if 0 bytes of padding are required, it means only
        # a single \x80 is required.

        padding_required     = 15 - (len(data) % 16)
        data = '%s\x80' % data
        data = '%s%s' % (data, '\x00' * padding_required)
        return data

def unpad_data(data): #from tutorial
        if not data:
                return data

        data = data.rstrip('\x00')
        if data[-1]=='\x80':
                return data[:-1]
        else:
                return data

def get_pswd():
        pswd = getpass.getpass('Please enter (and remember) a password: ' )
        return pswd

def gen_salt(): # from tutorial
        #returns truely random salt
	#uses salt_len global (above)
        return RNG.new().read(salt_len)
        

def gen_key(salt):
	#takes generated salt
	#requests user input password (get_mswd method)
	# returns a tuple key
        password = get_pswd()
        key = _pbkdf2_cryptography.pbkdf2_sha1(password, salt, length=32, iterations=16384)
        if len(key) < 32:
		raise ValueError('I did not generate key as expected')
	ksplit = (key[:len(key)/2],key[len(key)/2:])
	return ksplit

###############################

def encrypt_data(data,key,salt):
	# takes data to encrypt, a generated salt, and a gen key
	# returns ciphertext
        data = pad_data(data)
        cipher = AES.new(key,AES.MODE_XTS)
        try:
		ciphertext= cipher.encrypt(data)
	except AssertionError as e:
		print "ERROR in download.py/encrypt_data/cipher.encrypt(data)"
		traceback.print_stack()
		#traceback.print_last()
		sys.exit()
	return salt + ciphertext

def decrypt_data(ciphertext):
	#takes ciphertext, separates salt and ciphertext, generates key
	#returns unencrypted data
	#NOTE: does not handle invalid ciphertext (yet)
        salt = ciphertext[:salt_len]
        ciphertext = ciphertext[salt_len:]
        key = gen_key(salt)
        cipher = AES.new (key,AES.MODE_XTS)
        data = cipher.decrypt(ciphertext)
        return unpad_data(data)

#############################################

def test_main():
	s = "Hello world! I am some dummy text to test. I need at least 128 bits. This may be enough! \n"
	password1 = 'cat'
        password2 = 'dog'
        salt = gen_salt()
        key = gen_key(salt)
        ciphertext = encrypt_data(password1,key,salt)
        out1 = decrypt_data(ciphertext)
	if password1 == out1:
		print 'it works'
	else:
		print 'bad'

