"""
@kwalter
june 2016
file handling - downloads requested content, stores as cStringIO file, passes to appropriate encrypt/decrypt fcns in mycrypto
"""

import sys, os, signal
import dropbox
import cStringIO
from mycrypto import *
app_key = 'i5se1si6a9gaq3c'     #currently clouddropsweeper
app_secret = 'ni8hqn8mfyzbzdk'  #currently clouddropsweeper


######################

def quikauth():
        # for editing session, skips user sign in
        # every session requires generatinga new token

        print 'Authorizing Account Access: \n'

        access_token= '0iETCGWMGlAAAAAAAAAAZ6GPUHNwx3RnpZ-aAmEdRPsvmhnvuFYzGC_RVB6ooGTg'
        global client
        client = dropbox.client.DropboxClient(access_token)
        #print 'linked account: ', client.account_info()
        print '    I am connected to %s\'s account\n' % (client.account_info()['display_name'])
        return

def download(client,DBpath):
	print "Retrieving file: ", DBpath
	original = cStringIO.StringIO()
	try:
        	f, metadata = client.get_file_and_metadata(DBpath)
        except Exception as e:
		print 'I could not get the requested file (%s)'%DBpath
		return
	with f:
		original.write(f.read())
	rev = metadata['rev']
        return original,rev

def print_file(local_path):
	print 'content: ',local_path.getvalue()
	return

def encrypt_local(local_path):
	salt = gen_salt()
        key = gen_key(salt)
	ciphertext = cStringIO.StringIO()
	cdata = ''
	print 'Len local: %d \n len cdata: %d \n len ciphertext: %d'%(len(local_path.getvalue()),len(cdata),len(ciphertext.getvalue()))
	cdata = encrypt_data(local_path.getvalue(),key,salt)
	ciphertext.write(cdata)
	print 'Len local: %d \n len cdata: %d \n len ciphertext: %d'%(len(local_path.getvalue()),len(cdata),len(ciphertext.getvalue()))
	return ciphertext

def upload_chunked(client, local_file, DBpath, file_size):
        print "Uploading: ", file_size
        offset = 0
	fullpath = "/dropbox"+DBpath
        upload_id = None
        while offset < file_size: #bytes
        	print "looping"
                try:
			print "attempting chunk upload"
			#RETURNS TUPLE, NOT DICTIONARY LIKE DOCS SAY
               		offset,upload_id = client.upload_chunk(local_file,offset,upload_id)
               		print "did one!"
		except Exception, e:
			print "error: %s" %e
			break
			return None
        print "all chunks uploaded. Commiting"
	print 'fullpath:%s'%fullpath
	print 'upload_id:%s'%upload_id
	rtn = client.commit_chunked_upload(fullpath,upload_id) #
	return rtn 

def upload(client, local_file, DBpath, rev):
	local_file.seek(0, os.SEEK_END)
        fileSizeB = local_file.tell()
        print 'file size: ', fileSizeB>>20
        fileSizeMB = fileSizeB>>20
	print 150 <= fileSizeMB
	
	if 150 <= fileSizeMB:
		print "Upload via Chunked"
		response =  "Method not functioning, please try again later"
		#response = upload_chunked(client,local_file,DBpath,fileSizeB)
	else:
		print "Attempting regular file upload"
		response = client.put_file(DBpath,local_file,parent_rev=rev)
		print "UPLOADED: ",response
	return response

def decrypt_local(local_path):
	cleartext = cStringIO.StringIO()
	cleartext.write(decrypt_data(local_path.getvalue()))
	return cleartext

##########################

def test_chunked():
	quikauth()
	local_path = cStringIO.StringIO()
	with open("newfile.txt","rb") as f:
		local_path.write(f.read())
	DBpath = '/newfile.txt'
	try:
		rev = client.metadata("newfile.txt")['rev']
	except Exception, e:
		print 'File does not exist yet, loading new file'
		rev = None
	print upload(client, local_path,DBpath,rev)
	online = cStringIO.StringIO()
	online,rev = download(client, DBpath)
	if local_path.getvalue() == online.getvalue():
		print "We did it!"
	else:
		print "Not so much"
	local_path.close()
	online.close()

#test_chunked()

def test_encryption(local_path):
	with open(local_path,"rb") as f:
		original = f.read()
	salt = gen_salt()
	key = gen_key(salt)
	final = decrypt_data(encrypt_data(original,key,salt))
	if original == final:
		print "Correct encryption/decryption, continuing to encrypt"
	else:
		print "There was a problem, exiting program"
		sys.exit()

def test_all():
	quikauth()
	DBpath = "/testfile.txt"
	NEWITEM,rev = download(client, DBpath)
	print_file(NEWITEM)
	#test_encryption(NEWITEM) #test data encryption
	#sys.exit()
	ciphertext = encrypt_local(NEWITEM)
	print_file(NEWITEM)
	response = upload(client,NEWITEM, DBpath,rev)
	NEWITEM,rev = download(client,DBpath)
	print_file(NEWITEM)
	cleartext = decrypt_local(NEWITEM)
	print_file(NEWITEM)
	response = upload(client,NEWITEM,DBpath,rev)


