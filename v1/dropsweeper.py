# dropsweeper.py
# @kwalter june 2016
# asks user to sign in and authorize, locates all files matching current parameters,
# asks user to select all of found files to do an action, completes actions as requested
# auth, find, offer, action (delete, encrypt, delete)


#imports
import sys
import os
import dropbox
from datetime import datetime, timedelta
from dateutil.parser import parse
import pytz
import cStringIO
from mycrypto import *
from download import *

#globals
app_key = 'i5se1si6a9gaq3c'	#currently clouddropsweeper
app_secret = 'ni8hqn8mfyzbzdk'	#currently clouddropsweeper
today = datetime.now(pytz.utc)	#used in is_old
days = timedelta(days=0)	#used in is_old, change for different search settings! 	
# global client
# global files
# global ENCRYPT
# global DELETE
# global DECRYPT
# global actions

################### FRAMEWORK #########################

def auth():
	""" have user sign in, authorize access to account, confirm link
	"""

	print 'Authorizing Account Access: \n'

	# Have the user sign in and authorize this token
	flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key,app_secret)
	authorize_url = flow.start()
	print '1. Go to: ' + authorize_url
        print '2. Click "Allow" (you might have to log in first)'
        print '3. Copy the authorization code.'
        code = raw_input("Enter the authorization code here: ").strip()
      # if bad app key/secret, 400 error from link

        # This will fail if the user enters an invalid authorization code
        try:
                access_token, user_id = flow.finish(code)
        except:
                e = sys.exc_info()[0]
                print "Error: bad auth code"
                sys.exit()
	global client
	client = dropbox.client.DropboxClient(access_token)
	#print 'linked account: ', client.account_info()
	print '    I am connected to %s\'s account \n' % (client.account_info()['display_name'])
	return

def quikauth():
	""" for editing session, skips user sign in
	 every session requires generatinga new token
	"""

	print 'Authorizing Account Access: \n'

	access_token= '0iETCGWMGlAAAAAAAAAAZ6GPUHNwx3RnpZ-aAmEdRPsvmhnvuFYzGC_RVB6ooGTg'
	global client
	client = dropbox.client.DropboxClient(access_token)
        #print 'linked account: ', client.account_info()
	print '    I am connected to %s\'s account\n' % (client.account_info()['display_name'])
	return

def is_old(path):
	""" returns true if path item is older than days requested
	"""
	global today 
	mod = parse(client.metadata(path)['modified'])
	moddelta = today-mod
	if moddelta < days:
		#print '%s is less than %s' %(str(moddelta), str(days)) 
		return False
	return True

def find():
	""" open all folders from root of client, find all files matching criteria
	 sets list of paths to global
	"""
	print 'Finding all files (this may take some time, please wait) \n'

	folders = []
        global files
	files = []
        content = []

        folders.append('/')
        while len(folders) !=0 :
                #pop next folder
                content = client.metadata(folders[0])['contents']
                folders = folders[1:]

                #sort content
                for i in range(0,len(content)):
                        if content[i]['is_dir']:
                                folders.append(content[i]['path'])
                        else:
				#TODO search heuristics (mod date, content?)
             			if is_old(content[i]['path']):
					print '    found a matching file! Adding to list.'
					files.append(content[i]['path'])
				else:
					print '    file does not match criteria:', content[i]['path']
	return

def offer():
	""" takes global files, takes user input for each action
	 sets global list of indices wrt files to do actions
	"""	

	print "\nI have found the following %d files" % (len(files))
	for i in range(0,len(files)):
                #clean metadata print
                #toprint = str(client.metadata(files[i])).replace(", u",", \n")
                #toprint = toprint.replace("{u","{")
                #toprint = toprint.replace(": u",": ")

                print "    File Index: ", i
                #print toprint		#prints all meta
		print '    ', files[i]		#prints path only

	print
        print "I can do the following actions: DELETE, ENCRYPT, DECRYPT"
        print
	print "Select the files you would like to ENCRYPT."
	global ENCRYPT
	ENCRYPT = raw_input("Enter the file indices separated by a space: ").split()
	print
        print "Select the files you would like to DELETE."
	global DELETE
        DELETE = raw_input("Enter the file indices separated by a space: ").split()
	print
        print "Select the files you would like to DECRYPT."
	print "PLEASE NOTE: only decrypt files you have previously encrypted."
        global DECRYPT
        DECRYPT = raw_input("Enter the file indices separated by a space: ").split()

	return

def action():
	""" excute actions
	
	"""
	encrypt()
	delete()
	decrypt()
	print 'I have no more functions to complete!'
	return

def full_action():
	""" excute actions with framework included
	"""
	mkactframe()

	for ACTION in actions:
		print
		print 'Begin %s' % ACTION["text"]
		paths = ""
		for i in range(0,len(ACTION["input"])):
			try:
				paths = paths + files[int(ACTION["input"][i])]
				paths = paths + '\n'
			except:
				print 'I\'m sorry, %s was not a valid index. Skipping item.' %ACTION["input"][i]
		paths.strip()
		plist = paths.split("\n")
		plist = filter(None,plist)
		if 0==len(plist):
			print "I found no valid file indices. Skipping to next action."
			continue
		print 'you have selected the following files to %s:'%ACTION["text"]
		print '\n'.join(map(str, plist))
		check = raw_input('\nAre you sure you want to %s these files? (y/n) '%ACTION["text"])
		while True:
			if "y"==check:
				#plist = paths.split('\n')
				for DBpath in plist:
					#ATTEMPT ACTION
					#IF FAILURE, make a note
					print DBpath
					print ACTION["function"]
					globals()[ACTION["function"]](DBpath)
					#######TODO MAKE SAFE
				print '%d files were %s'%(len(plist),ACTION["done"])
				break
			if "n"==check:
				print 'Files were not %s' %ACTION["done"]
				break
			else:
				print 'I\'m sorry, I didn\'t understand. Please enter \'y\' or \'n\'.'
	                        check = raw_input("\nAre you sure you want to encrypt these files? (y/n) ")
		print "Moving to next action."
	return


def mkactframe(): #ACTION[""] - input, done, text, function
	""" make nice list for all elements of action, action helper
	"""
	global DELETE
	dDELETE = {}
	dDELETE["input"]=DELETE
	dDELETE["done"]="deleted"
	dDELETE["text"]="delete"
	dDELETE["function"]="delete_safe"
	global ENCRYPT
        dENCRYPT = {}
        dENCRYPT["input"]=ENCRYPT
        dENCRYPT["done"]="encrypted"
        dENCRYPT["text"]="encrypt"
        dENCRYPT["function"]="encrypt_safe"
	global DECRYPT
        dDECRYPT = {}
        dDECRYPT["input"]=DECRYPT
        dDECRYPT["done"]="decrypted"
        dDECRYPT["text"]="decrypt"
        dDECRYPT["function"]="decrypt_safe"
	global actions
	actions = []
	actions.append(dDELETE)
	actions.append(dENCRYPT)
	actions.append(dDECRYPT)
	return

################### ACTIONS #########################

def encrypt_safe(DBpath):
	#try:
	original,rev = download(client,DBpath)
        	#print_file(NEWITEM)
        	#test_encryption(NEWITEM) #test data encryption
        	#sys.exit()
        ciphertext = encrypt_local(original)
       		#print_file(NEWITEM)
        #except Exception as e:
	#	print 'There was an error: %s' % e
	#	print 'No changes were made to your file (%s)'%DBpath
	#	try:
       #                 original.close()
       #                cleartext.close()
        #        except Exception as e:
        #                pass
	#	return
	try:
		response = upload(client,ciphertext, DBpath,rev)
	        print '    %s successfully encrypted!'% DBpath
	except Exception as e:
		print 'There was an error: %s' %e
		try:
                        client.restore(DBpath,rev)
                        print "Your file has been restored."
                except Exception as e:
                        print 'We cannot restore your file (%s)' % DBpath

        original.close()
        ciphertext.close()
	return

def encrypt():
	""" encrypt files in global ENCRYPT list with user confirmation
	"""

        bad = []
        print "\nYou have selected the following files to ENCRYPT:"
        for i in range(0,len(ENCRYPT)):
                try:
                        #print client.metadata(files[int(ENCRYPT[i])])   #prints all meta
                        print '   ', files[int(ENCRYPT[i])]                      #prints path only
                except:
                        #bad index
                        bad.append(i)
                        print 'I\'m sorry, %s was not a valid index. Skipping item.' %ENCRYPT[i]
        if len(bad) == len(ENCRYPT):
                print '    I\'m sorry, I found no files specified.'
                print '\nNo files encrypted.'
                return
        check = raw_input("\nAre you sure you want to encrypt these files? (y/n) ")
        while True:
                if check == "y" :
                        for i in range(0,len(ENCRYPT)):
                                if i not in bad:
                                        DBpath = client.metadata(files[int(ENCRYPT[i])])['path']
					NEWITEM,rev = download(client,DBpath)
        				print_file(NEWITEM)
        				#test_encryption(NEWITEM) #test data encryption
        				#sys.exit()
        				ciphertext = encrypt_local(NEWITEM)
        				print_file(NEWITEM)
        				response = upload(client,NEWITEM, DBpath,rev)
					print '    %s successfully encrypted!'% DBpath
					os.remove(NEWITEM)
			print '%d files encrypted.' % (len(ENCRYPT)-len(bad))
			#TODO provide directions for decrypt??
                        break
                if check == "n" :
                        print 'No files encrypted.'
                        break
                else:
                        print 'I\'m sorry, I didn\'t understand. Please enter \'y\' or \'n\'.'
                        check = raw_input("\nAre you sure you want to encrypt these files? (y/n) ")
	return

#NOTE: check for required chunked?

def decrypt_safe(DBpath):
	try:
		original,rev = download(client, DBpath)
        	#print_file(NEWITEM)
        	cleartext = decrypt_local(original)
        	#print_file(NEWITEM)
        except Exception as e:
		print 'There was an error: %s' % e
                print 'No changes were made to your file (%s)'%DBpath
                try:
			original.close()
                	cleartext.close()
                except Exception as e:
			pass
		return
	try:
		response = upload(client,cleartext,DBpath,rev)
		print '    %s successfully decrypted!'% DBpath
	except Exception as e:
		print 'There was an error: %s' % e
                try:
                        client.restore(DBpath,rev)
                        print "Your file has been restored."
                except Exception as e:
                        print 'We cannot restore your file (%s)' % DBpath

	original.close()
        cleartext.close()
	return

def decrypt():
	"""decrypt files in global DECRYPT list with user confirmation
	"""

        bad = []
        print "\nYou have selected the following files to DECRYPT:"
        for i in range(0,len(DECRYPT)):
                try:
                        #print client.metadata(files[int(DECRYPT[i])])   #prints all meta
                        print '   ', files[int(DECRYPT[i])]                      #prints path only
                except:
                        #bad index
                        bad.append(i)
                        print 'I\'m sorry, %s was not a valid index. Skipping item.' %DECRYPT[i]
        if len(bad) == len(DECRYPT):
                print '    I\'m sorry, I found no files specified.'
                print '\nNo files deleted.'
                return
        check = raw_input("\nAre you sure you want to decrypt these files? (y/n) ")
        while True:
                if check == "y" :
                        for i in range(0,len(DECRYPT)):
                                if i not in bad:
                                        DBpath = client.metadata(files[int(DECRYPT[i])])['path']
                                        NEWITEM,rev = download(client, DBpath)
        				print_file(NEWITEM)
        				cleartext = decrypt_local(NEWITEM)
        				print_file(NEWITEM)
        				response = upload(client,NEWITEM,DBpath,rev)
					os.remove(NEWITEM)
                                        print '    %s successfully decrypted!'% DBpath
                        print '%d files decrypted.' % (len(DECRYPT)-len(bad))
                        break
                if check == "n" :
                        print 'No files decrypted.'
                        break
                else:
                        print 'I\'m sorry, I didn\'t understand. Please enter \'y\' or \'n\'.'
                        check = raw_input("\nAre you sure you want to decrypt these files? (y/n) ")
        return

def delete_safe(DBpath):
	try:
		rev = client.get_metadata(DBpath)['rev']
		client.file_delete(DBpath)
		print '    %s successfully deleted!'% DBpath
	except Exception as e:
		print 'There was an error: %s' % e
		try:
			client.restore(DBpath,rev)
			print "Your file has been restored."
		except Exception as e:
			print 'We cannot restore your file (%s)' % DBpath
	return

def delete():
	"""delete files in global DELETE list with user confirmation
	"""

	bad = []
	print "\nYou have selected the following files to DELETE:"
        for i in range(0,len(DELETE)):
        	try:
               		#print client.metadata(files[int(DELETE[i])])	#prints all meta
			print '   ', files[int(DELETE[i])]			#prints path only
        	except:
			#bad index
			bad.append(i)
			print 'I\'m sorry, %s was not a valid index. Skipping item.' %DELETE[i]
	if len(bad) == len(DELETE):
		print '    I\'m sorry, I found no files specified.'
		print '\nNo files deleted.'
		return
	check = raw_input("\nAre you sure you want to delete these files? (y/n) ")
	while True:
		if check == "y" :
			for i in range(0,len(DELETE)):
                	        if i not in bad:
					path = client.metadata(files[int(DELETE[i])])['path']
         	 	              	#client.file_delete(path) #
			print '%d files deleted.' % (len(DELETE)-len(bad))
			break
		if check == "n" :
			print 'No files deleted.'
			break
        	else:
			print 'I\'m sorry, I didn\'t understand. Please enter \'y\' or \'n\'.'
			check = raw_input("\nAre you sure you want to delete these files? (y/n) ")
	return

################### MAIN ###########################

def main():
	print "I am Dropsweeper"
	auth()		#uncomment for full login function
	#quikauth()	#comment for full login function
	find()
	offer()
	full_action()
	print 'Goodbye.'
	
if __name__ == '__main__':
	main()

#######################################################################
######### END OF FILE #################################################
