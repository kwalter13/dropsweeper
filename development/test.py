"""
@kwalter
This file (test.py) is for testing new elements. don't put permanent code here!

"""

import unittest
import getpass
import sys
import Crypto.Random.OSRNG.posix as RNG
from onepassword import _pbkdf2_cryptography
from CryptoPlus.Cipher import AES

#NOTES
#AES - block encryption. make padding/unpadding fcns to make plaintext 16byte blocks. Key must be tuple, 16 bits. Recommended # of iterations in tutorial 16384. 
#salt must be random, NOT just unique (do not use rand fcn). public, nonsecret. pass in ciphertext. min of 16bytes.
# key comes from 1pass
# cipher from cryptoplus

salt_len=32

def pad_data(data): #method from tutorial

	# return data if no padding is required
	if len(data) % 16 == 0: 
        	return data

	# subtract one byte that should be the 0x80
	# if 0 bytes of padding are required, it means only
	# a single \x80 is required.

	padding_required     = 15 - (len(data) % 16)
	data = '%s\x80' % data
	data = '%s%s' % (data, '\x00' * padding_required)
	return data

def unpad_data(data): #from tutorial
	if not data:
		return data
	
	data = data.rstrip('\x00')
	if data[-1]=='\x80':
		return data[:-1]
	else:
		return data

def get_pswd():
        pswd = getpass.getpass('Please enter (and remember) a password: ' )
        return pswd

def gen_salt():
        #see pycrypto example page#
	return RNG.new().read(salt_len)
        #return "12345678"

def gen_key(salt,password):
        password = get_pswd()
        key = _pbkdf2_cryptography.pbkdf2_sha1(password, salt, length=32, iterations=16384)
        ksplit = (key[:len(key)/2],key[len(key)/2:])
	return ksplit


def encrypt(data,key,salt):
	data = pad_data(data)
	cipher = AES.new(key,AES.MODE_XTS)
	ciphertext= cipher.encrypt(data)
	return salt + ciphertext

def decrypt(ciphertext,password):
	#if len(ciphertext) <= AES.block_size:
	#	raise Exception("Invalid ciphertext.")
	salt = ciphertext[:salt_len]
	ciphertext = ciphertext[salt_len:]
	#password = get_pswd()
	key = gen_key(salt,password)
	cipher = AES.new (key,AES.MODE_XTS)
	data = cipher.decrypt(ciphertext)
	return unpad_data(data)

class TestStringMethods(unittest.TestCase):
	
	def test_encrypt(self):
		s = "Hello world! I am some dummy text to test. I need at least 128 bits. This may be enough! \n"
        	password1 = 'cat'
		password2 = 'dog'
		salt = gen_salt()
		key = gen_key(salt,password1)
		ciphertext = encrypt(s,key,salt)
		
		out1 = decrypt(ciphertext,password1)
		#out2 = decrypt(ciphertext,password2)
		self.assertEqual(s,out1)
		#self.assertNotEqual(s,out2)

if __name__ == '__main__':
	unittest.main()
