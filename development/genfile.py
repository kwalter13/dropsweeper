import os

file_size = 150<<20

content = 'a'*2000

with open("newfile.txt","wb") as f:
	f.seek(0, os.SEEK_END)
	while f.tell()<file_size :
		print "Not long enough"
		f.write(content)
