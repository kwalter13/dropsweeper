# NOTE: not entirely patched for api v2!
"""
dropsweeper.py
@kwalter june 2016
asks user to sign in and authorize, locates all files matching current parameters, asks user to select all of found files to do an action, completes actions as requested
auth, find, offer, action (delete, encrypt, delete)
"""

#imports
import sys,os, pytz,cStringIO, dropbox
from datetime import datetime, timedelta
#TODO figure out importfrom dateutil.parser import parse
#from mycrypto import *
from handler import *
from dropbox import DropboxOAuth2FlowNoRedirect,Dropbox,files

#globals
app_key = 'i5se1si6a9gaq3c'	#currently clouddropsweeper
app_secret = 'ni8hqn8mfyzbzdk'	#currently clouddropsweeper
today = datetime.utcnow()	#used in is_old
days = timedelta(days=0)	#used in is_old, change for different search settings! 	
AGE = False
IMAGE = False
# global client 	#dropbox item
# global user_id 	#string
# global content 	#paginated list of all items meta, root recursive
# global results 	#list of items meta that fit search terms, from content
# global ENCRYPT
# global DELETE
# global DECRYPT
# global actions

################### FRAMEWORK #########################

def auth(): 
	# have user sign in, authorize access to account, confirm link

	print 'Authorizing Account Access: \n'

	# Have the user sign in and authorize this token
	flow = DropboxOAuth2FlowNoRedirect(app_key,app_secret) #confirmed
	authorize_url = flow.start() #confirmed
	print '1. Go to: ' + authorize_url
        print '2. Click "Allow" (you might have to log in first)'
        print '3. Copy the authorization code.'
        code = raw_input("Enter the authorization code here: ").strip()
      # if bad app key/secret, 400 error from link
	global user_id
        # This will fail if the user enters an invalid authorization code
        try:
                access_token, user_id = flow.finish(code) #confirmed
        except Exception, e:
                print "Error: bad auth code"
                sys.exit()
	global client
	client = Dropbox(access_token)
	print '    I am connected to %s\'s account \n' % client.users_get_current_account().name.display_name #confirmed
	return

def quikauth(): 
	# for editing session, skips user sign in
	# every session requires generatinga new token
	
	print 'Authorizing Account Access: \n'

	access_token= '0iETCGWMGlAAAAAAAAAAZ6GPUHNwx3RnpZ-aAmEdRPsvmhnvuFYzGC_RVB6ooGTg'
	global client
	client = Dropbox(access_token) #confirmed
	print '    I am connected to %s\'s account \n' % client.users_get_current_account().name.display_name #confirmed
	return

def is_old(item):
	#returns true if  item is older than days requested
	global today 
	mod = item.server_modified
	moddelta = today-mod
	if moddelta < days:
		#print item.name
		#print '%s is less than %s' %(str(moddelta), str(days)) 
		return False
	print item.name
        print '%s is MORE than %s' %(str(moddelta), str(days))

	return True

def _paginate():
	#create paginated list (content) of all files and folder meta, root recursive
        content.append(client.files_list_folder('',recursive=True))
        i = 0
        while content[i].has_more:
                cursor = content[i].cursor
                content.append(client.files_list_folder_continue(cursor))
                i += 1

def _print_pages():
	#print all paginated content root recursive
        for i in range(0,len(content)):
                print
                print "######PAGE#######"
                print type(content[i].entries)
                for j in range(0,len(content[i].entries)):
                        print
                        print content[i].entries[j]

def _search_heuristics():
	global AGE
	global IMAGE
	#ask user for search parameters, set accordingly
	print "I can search for files with the following characteristics."
	print "Please indicate which you would like me to use:"
	while True:
		age = raw_input("AGE: last modified date over DAYS old (y/n) ")
		if "y" == age:
			AGE = True
			#print "I will search by age"
			break
		elif "n" == age:
			pass
			break
		else:
			print "I did not understand that, please input 'y' or 'n'"
	while True:
                image = raw_input("Image: use google Vision to search images (y/n) ")
                if "y" == image:
                        IMAGE = True
			#print "I will search image content"
                        break
                elif "n" == image:
                        pass
                        break
                else:
                        print "I did not understand that, please input 'y' or 'n'"

def _search_content():
	#find alzal files within content that fit the selected criteria
	global results
	results = []
	for i in range(0,len(content)):
		for j in range(0,len(content[i].entries)):
			item = content[i].entries[j]
			if isinstance(item, dropbox.files.FolderMetadata):
				#print "I am a folder"
				pass
			else:
				#print "I am not a folder"
				if AGE and is_old(item):
						results.append(item)
				if IMAGE:
					pass
	print "I have completed all search parameters"

def find():
	#open all folders from root of client, find all files matching criteria
	#sets result list

	print 'Finding all files (this may take some time, please wait) \n'

        global content
        content = [] #all pages of files and folders from root
	
	_paginate()
	#_print_pages()
	_search_heuristics()
	_search_content()
	#_print_file_meta(results)
		
def _print_file_meta(metalist):
	for i in range(0, len(metalist)):
                print i
                print '    ',metalist[i].name
		print '    ',metalist[i].path_lower
		#print '    ',"Client:",metalist[i].client_modified,"Server:",metalist[i].server_modified

def offer():
	#takes global results, takes user input for each action
	#sets global list of indices wrt results to do actions

	print "\nI have found the following %d files" % (len(results))
	_print_file_meta(results)	

	print
        print "I can do the following actions: ENCRYPT, DELETE, DECRYPT"
        print
	print "Select the files you would like to ENCRYPT."
	global ENCRYPT
	ENCRYPT = raw_input("Enter the file indices separated by a space: ").split()
	print
        print "Select the files you would like to DELETE."
	global DELETE
        DELETE = raw_input("Enter the file indices separated by a space: ").split()
	print
        print "Select the files you would like to DECRYPT."
	print "PLEASE NOTE: only decrypt files you have previously encrypted."
        global DECRYPT
        DECRYPT = raw_input("Enter the file indices separated by a space: ").split()
	return

def full_action():
	#excute actions with framework included
	
	_mkactframe()

	for ACTION in actions:
		print
		print 'Begin %s' % ACTION["text"]
		print 'you have selected the following files to %s:'%ACTION["text"]
		BAD = []
		for s in ACTION["input"]:
			try:
				item = results[int(s)]
				print item.name
			except:
				print 'I\'m sorry, %s was not a valid index. Removing input.' %s
				BAD.append(s)
		for s in BAD:
			ACTION["input"].remove(s)	
		if 0==len(ACTION["input"]):
			print 'You requested no valid files to %s'%ACTION["text"]
			print "Moving to next action."
			continue
                while True:
			check = raw_input('\nAre you sure you want to %s these files? (y/n) '%ACTION["text"]).strip()   
                        if "y"==check:
				count = 0
				for s in ACTION["input"]:
					item = results[int(s)]
					rev = item.rev
					DBpath = item.path_lower
					result = globals()[ACTION["function"]](DBpath,rev)
					if result:
						count += 1
					else:
						print 'I\'m sorry, your file could not be %s.'%ACTION["done"]
				plural = "s"
				if 1==count:
					plural = ""
				print '%d file%s %s'%(count,plural,ACTION["done"])
				break

                        elif "n"==check:
				print 'Files were not %s' %ACTION["done"]
                                break
                        else:
                                print 'I\'m sorry, I didn\'t understand. Please enter \'y\' or \'n\'.'
                print "Moving to next action."
        return


def _mkactframe(): #ACTION[""] - input, done, text, function
	#make nice list for all elements of action, action helper
	
	global DELETE
	dDELETE = {}
	dDELETE["input"]=DELETE
	dDELETE["done"]="deleted"
	dDELETE["text"]="delete"
	dDELETE["function"]="delete_safe"
	global ENCRYPT
        dENCRYPT = {}
        dENCRYPT["input"]=ENCRYPT
        dENCRYPT["done"]="encrypted"
        dENCRYPT["text"]="encrypt"
        dENCRYPT["function"]="encrypt_safe"
	global DECRYPT
        dDECRYPT = {}
        dDECRYPT["input"]=DECRYPT
        dDECRYPT["done"]="decrypted"
        dDECRYPT["text"]="decrypt"
        dDECRYPT["function"]="decrypt_safe"
	global actions
	actions = []
	actions.append(dDELETE)
	actions.append(dENCRYPT)
	actions.append(dDECRYPT)
	return

################### ACTIONS #########################

def encrypt_safe(DBpath,rev):
	
	return
	
	#try:
	original,rev = download(client,DBpath)
        	#print_file(NEWITEM)
        	#test_encryption(NEWITEM) #test data encryption
        	#sys.exit()
        ciphertext = encrypt_local(original)
       		#print_file(NEWITEM)
        #except Exception as e:
	#	print 'There was an error: %s' % e
	#	print 'No changes were made to your file (%s)'%DBpath
	#	try:
       #                 original.close()
       #                cleartext.close()
        #        except Exception as e:
        #                pass
	#	return
	try:
		response = upload(client,ciphertext, DBpath,rev)
	        print '    %s successfully encrypted!'% DBpath
	except Exception as e:
		print 'There was an error: %s' %e
		try:
                        client.restore(DBpath,rev)
                        print "Your file has been restored."
                except Exception as e:
                        print 'We cannot restore your file (%s)' % DBpath

        original.close()
        ciphertext.close()
	return

#NOTE: check for required chunked?

def decrypt_safe(DBpath,rev):
	try:
		original,rev = download(client, DBpath)
        	#print_file(NEWITEM)
        	cleartext = decrypt_local(original)
        	#print_file(NEWITEM)
        except Exception as e:
		print 'There was an error: %s' % e
                print 'No changes were made to your file (%s)'%DBpath
                try:
			original.close()
                	cleartext.close()
                except Exception as e:
			pass
		return
	try:
		response = upload(client,cleartext,DBpath,rev)
		print '    %s successfully decrypted!'% DBpath
	except Exception as e:
		print 'There was an error: %s' % e
                try:
                        client.restore(DBpath,rev)
                        print "Your file has been restored."
                except Exception as e:
                        print 'We cannot restore your file (%s)' % DBpath

	original.close()
        cleartext.close()
	return

def delete_safe(DBpath,rev):
	try:
		item = client.files_delete(DBpath)
		check = client.files_get_metadata(DBpath,include_deleted)
		if isinstance(check,dropbox.files.DeletedMetadata):
			print '    %s successfully deleted!'% DBpath
			return True
		else:
			print '    %s was not deleted'% DBpath
			return False
	except Exception as e:
		print 'There was an error: %s' % e
		try:
			print 'Attempting to restore %s to version %s'%(DBpath,rev)
			client.files_restore(DBpath,rev)
			print "Your file has been restored."
		except Exception as e:
			print 'We cannot restore your file (%s)' % DBpath
	return False


################### TESTS #########################

def _download():
	pass

def _newupload():
	newfile = cStringIO.StringIO("Hello, World!")
	response = upload(client,newfile,"/newfile")
	print "TEST newupload:",response

def _tests():
	_download()	
	_newupload()

################### MAIN ###########################

def main():
	print "I am Dropsweeper"
	#auth()		#uncomment for full login function
	quikauth()	#comment for full login function
	find()
	_print_file_meta(results)
	#_tests()
	offer()
	full_action()
	print 'Goodbye.'
	
if __name__ == '__main__':
	main()

#######################################################################
######### END OF FILE #################################################
