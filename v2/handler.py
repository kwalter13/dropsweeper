"""
@kwalter
july 2016
file handler - downloads/uploads, stores locally as cStringIO file, passes to data manipulators (encrypt/decrypt)
updated 'download.py' for v2
"""

import sys,os, dropbox, cStringIO

app_key = 'i5se1si6a9gaq3c'     #currently clouddropsweeper
app_secret = 'ni8hqn8mfyzbzdk'  #currently clouddropsweeper


def quikauth():
        # for editing session, skips user sign in
        # every session requires generatinga new token

        print 'Authorizing Account Access: \n'

        access_token= '0iETCGWMGlAAAAAAAAAAZ6GPUHNwx3RnpZ-aAmEdRPsvmhnvuFYzGC_RVB6ooGTg'
        global client
        client = Dropbox(access_token) #confirmed
        print '    I am connected to %s\'s account \n' % client.users_get_current_account().name.display_name #confirmed
        return

#########################################

def download(client,DBpath):
	print "Retrieving file:", DBpath
	new = cStringIO.StringIO()
	meta,response = client.files_download_to_file(new,DBpath)
	return new, meta.rev

def _get_size(localfile):
	localfile.seek(0,os.SEEK_END)
	sizeBytes = localfile.tell()
	sizeMB = sizeBytes>>20
	return sizeBytes,sizeMB

def upload(client,localfile, DBpath, rev=None):
	sizeBytes,sizeMB=_get_size(localfile)
	if 150< sizeMB:
		print "File is larger than 150 MB"
		print "I cannot upload this yet"
		#TODO complete large file upload
		return upload_large()
	print 'Uploading local file to %s with version %s'%(DBpath,rev)
	response = None
	if None==rev:
		response = client.files_upload(localfile,DBpath)
	response = client.files_upload(localfile,DBpath,mode=WriteMode('update',rev))
	if None!=response:	
		return True
	return False

def upload_large():
	return False

def print_file(localpath):
	pass

#########################################

def encrypt_local():
	pass

def decrypt_local():
	pass

